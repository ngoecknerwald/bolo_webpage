<?php 
$rootPrefix="../";
$siteUniqId="5334aac3933db";
$siteFriendlyId="polarbearv2";
$pageUniqId="5339adae4589e";
$pageFriendlyId="index";
$pageTypeUniqId="5339ac1b7d389";
$language="en";
include '../site.php';
?><!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="<?php print _("Measuring the CMB Polarization with POLARBEAR"); ?>">
	<meta name="author" content="Thomas Tu">
  <link rel="shortcut icon" href="/respond/sites/polarbearv2/files/acquia_slate_logo.png">

	<title>POLARBEAR</title>

  <link href="../css/carousel.css" rel="stylesheet">
    <link href="//netdna.bootstrapcdn.com/bootswatch/3.0.0/cerulean/bootstrap.min.css" rel="stylesheet">

	<!-- css -->
<link href="../css/content.css?v=2.8.3" type="text/css" rel="stylesheet" media="screen">
<link href="//cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.css" type="text/css" rel="stylesheet" media="screen">
<link href="//netdna.bootstrapcdn.com/font-awesome/4.0.2/css/font-awesome.css" rel="stylesheet">
<link href="../css/prettify.css?v=2.8.3" type="text/css" rel="stylesheet" media="screen">
</head>

<body data-siteuniqid="5334aac3933db" data-sitefriendlyid="polarbearv2" data-domain="bolo.berkeley.edu/polarbear" data-pageuniqid="5339adae4589e" data-pagefriendlyid="index" data-pagetypeuniqid="5339ac1b7d389" data-api="localhost:8888/respond">

<!--Menu-->
<div class="navbar-wrapper">
<div class="container">

	<div class="navbar navbar-inverse navbar-static-top" role="navigation">
	<div class="container">
		<!--Make the menu mobile friendly-->
		<div class="navbar-header" >
		<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
		<span class="sr-only"></span>
		<span class="icon-bar"></span>
		<span class="icon-bar"></span>
		<span class="icon-bar"></span>
		</button>
          <a class="navbar-brand" href="//bolo.berkeley.edu/polarbear">POLARBEAR WITH THE HUAN TRAN TELESCOPE | Measuring Polarization of the CMB at the James Ax Observatory</a>
          </div>
	
	<div class="navbar-collapse collapse">
	 <ul class="nav navbar-nav" style="width:90%; margin-left: 5%;">
  		<li><?php print _("<a href=\"../././\">Home</a>"); ?></li><li><?php print _("<a href=\"../newsfeed\">Newsfeed</a>"); ?></li><li><?php print _("<a href=\"../technology\">Technology</a>"); ?></li><li><?php print _("<a href=\"../site\">Site</a>"); ?></li><li><?php print _("<a href=\"../future-plans\">Future Plans</a>"); ?></li><li><?php print _("<a href=\"../people\">People</a>"); ?></li><li><?php print _("<a href=\"../publications\">Publications</a>"); ?></li><li><?php print _("<a href=\"../support\">Support</a>"); ?></li><li><?php print _("<a href=\"../data\">Data</a>"); ?></li>
       </ul>
      </div>
	</div> 
	</div>
</div>
</div>
  
  

<div class="panel panel-default">
<div class="panel-body">
<div id="block-2" class="block row" data-nested="not-nested" data-containerid="" data-containercssclass=""><div class="col col-md-12"><style>
.wrapper{
display: inline-block;
text-align:center;
margin-bottom: .5%;
margin-top: .25%;
margin-left:.25%;
margin-right: .25%;
padding: .05%;
/*vertical-align:top;
min-width: 49%;*/
}

.institutionname > h4{
padding: 0px;
display: inline-block;
margin:0px;
text-align: center;
vertical-align: bottom;
}

.institutionname{
position:relative;
display:inline-block;
margin-left: -70px;
padding: 0px;
overflow: hidden;
max-width: 135px;
min-height: 50px;
top: -85px;
right: -45px;
-webkit-transform: rotate(-90deg);
-moz-transform: rotate(-90deg);
-ms-transform: rotate(-90deg);
-o-transform: rotate(-90deg);
filter: progid:DXImageTransform.Microsoft.BasicImage(rotation=3);
}

.frame{
display: inline-block;
overflow: hidden;
margin-left: .5%;
margin-right: .5%;
margin-top: .25%;
margin-bottom: .1%;
overflow: hidden;
border-radius: 5px;
position: relative;
z-index:5;
}

.portrait{
background-position: center;
background-size: cover;
background-repeat: no-repeat;
width: 110px; 
height: 140px; 
}


.name{
z-index: 10;
max-width: 110px;
height: 40px;
border-bottom: 1px dotted black;
}

.backgroundtext{
position: absolute;
top: 60px;
left: 0;
bottom: 0;
right: 0;
z-index: -1;
overflow: hidden;
color: black;
-webkit-user-select: none; /* Chrome/Safari */        
-moz-user-select: none; /* Firefox */
-ms-user-select: none; /* IE10+ */
user-select: none; 
}

/*hover behaviors*/
.frame:hover .name{
	border-bottom: 1px solid black;
	color: rgba(0,0,204,1);
}

</style>

<?php

$people = array(array("AstroParticule et Cosmologie",array( array("Giulio","Fabbian"),array("Maude","LeJeune"),array("Davide","Poletti"),array("Julien","Peloton"),array("Radek","Stompor") )),
array("Austin College",array( array("Peter","Hyland"))),
array("UC Berkeley", array( array("Ari","Cukierman"),array("Bill","Holzapfel"),array("Adrian","Lee"),array("Mike","Myers"),array("Christian","Reichardt"),array("Paul","Richards"),array("Bryan","Steinbach"),array("Blake","Sherwin"),array("Oliver","Zhan"),array("Aritoki","Suzuki"))),
array("Cardiff University", array( array("Peter","Ade"))),
array("Chile Team", array( array("Jose","Cortes"),array("Nolberto","Oyarce"))),
array("University of Colorado at Boulder",array(array("Nils","Halverson"),array("Greg","Jaehnig"),array("David","Schenck"))),
array("Dalhousie University",array(array("Scott","Chapman"),array("Colin","Ross"),array("Kaja","Rotermund"))),
array("Imperial College",array( array("Andrew","Jaffe"))),
array("Kalvi Institute",array(array("Nobu", "Katayama"),array("Haruki","Nishino"))),
array("KEK Institute",array(array("Yoshiki","Akiba"),array("Yuji","Chinone"),array("Masaya","Hasegawa"),array("Kaori","Hattori"),array("Masashi","Hazumi"),array("Hideki","Morii"),array("Takayuki","Tomaru"),array("Hiroshi","Yamaguchi"),array("Satoru","Takakura"),array("Yuki","Inoue"),array("Yasuto","Hori"),array("Tomotake","Matsumura"),array("Takahiro","Okamura"))),
array("Lawrence Berkeley National Lab",array( array("Julian","Borrill"),array("Josquin", "Errard"),array("Reijo","Keskitalo"),array("Theodore","Kisner"),array("Eric","Linder"))),
array("McGill University",array(array("Matt","Dobbs"))),
array("NASA Goddard Space Flight Center",array( array("Nathan","Miller"))),
array("Princeton University", array(array("Zigmund","Kermish"))),
array("Rutherford University",array(array("William","Grainger"))), array("Tel Aviv University",array(array("Meir","Shimon"))),
array("UC San Diego",array(array("Kam","Arnold"),array("Matt","Atlas"),array("Darcy","Barron"),array("David","Boettger"),array("Tucker","Elleflot"),array("Chang","Feng"),array("George","Fuller"),array("Brian","Keating"),array("Fred","Matsuda"),array("Stephanie","Moyerman"),array("Hans","Paar"),array("Praween","Siritanasak"),array("Nathan","Stebor"),array("Brandon","Wilson"),array("Christopher","Aleman"),array("Jennifer","Lawrence"),array("Daniel","Gonzales"))));

echo "<div class=\"wrapper\">";
for( $i = 0; $i < count($people); $i=$i+1) {
/*echo "<div class=\"wrapper\">*/
echo "
<div class=\"institutionname\"><h4>" .$people[$i][0]. "</h4></div>";
for($j = 0; $j < count($people[$i][1]) ; $j=$j+1) {
echo "<div class=\"frame\"><div class=\"backgroundtext\">No Photo Available</div><div class=\"portrait\" style=\"
background-image: url('http://cosmology.ucsd.edu/~thomastu/Polarbeartest/sites/default/files/AboutUs/AboutUs_thumb/".$people[$i][1][$j][0].$people[$i][1][$j][1].".jpg');\" ></div><div class=\"name\">".$people[$i][1][$j][0]." ". $people[$i][1][$j][1]."</div></div>";
}
/*echo "<br></div>";*/
}
echo "<br></div>";
?></div></div>

</div>
</div>
        
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
  <script src="../js/bootstrap.min.js"></script>
  <script src="../js/docs.min.js"></script>
</body>
</html>

<?php 
$rootPrefix="";
$siteUniqId="5334aac3933db";
$siteFriendlyId="polarbearv2";
$pageUniqId="5334aac3cabf6";
$pageFriendlyId="index";
$pageTypeUniqId="-1";
$language="en";
include 'site.php';
?><!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="<?php print _("Measuring the CMB Polarization with POLARBEAR"); ?>">
	<meta name="author" content="Thomas Tu">
	<link rel="shortcut icon" href="files/acquia_slate_logo.png">

	<title>POLARBEAR</title>

  <link href="css/carousel.css" rel="stylesheet">
    <link href="//netdna.bootstrapcdn.com/bootswatch/3.0.0/cerulean/bootstrap.min.css" rel="stylesheet">


</head>

<body data-siteuniqid="5334aac3933db" data-sitefriendlyid="polarbearv2" data-domain="bolo.berkeley.edu/polarbear" data-pageuniqid="5334aac3cabf6" data-pagefriendlyid="index" data-pagetypeuniqid="-1" data-api="localhost:8888/respond">

<!--Menu-->
<div class="navbar-wrapper">
<div class="container">

	<div class="navbar navbar-inverse navbar-static-top" role="navigation">
	<div class="container">
		<!--Make the menu mobile friendly-->
		<center><div class="navbar-header">
		<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
		<span class="sr-only"></span>
		<span class="icon-bar"></span>
		<span class="icon-bar"></span>
		<span class="icon-bar"></span>
		</button>
		<a class="navbar-brand" href="//bolo.berkeley.edu/polarbear">POLARBEAR WITH THE HUAN TRAN TELESCOPE | Measuring Polarization of the CMB at the James Ax Observatory</a>
          </div></center>
	
	<div class="navbar-collapse collapse">
	<ul class="nav navbar-nav" style="width:90%; margin-left: 5%;">
		<li class="active"><?php print _("<a href=\"././\">Home</a>"); ?></li><li><?php print _("<a href=\"newsfeed\">Newsfeed</a>"); ?></li><li><?php print _("<a href=\"technology\">Technology</a>"); ?></li><li><?php print _("<a href=\"site\">Site</a>"); ?></li><li><?php print _("<a href=\"future-plans\">Future Plans</a>"); ?></li><li><?php print _("<a href=\"people\">People</a>"); ?></li><li><?php print _("<a href=\"publications\">Publications</a>"); ?></li><li><?php print _("<a href=\"support\">Support</a>"); ?></li><li><?php print _("<a href=\"data\">Data</a>"); ?></li>
	</ul>
	</div>
	</div>
	</div>
</div>
</div>
  
  

<div class="panel panel-default" style="margin-left:7.5%; margin-right: 7.5%;">
<div class="panel-body">
	<div id="index-block-5" class="block row" data-nested="not-nested" data-containerid="" data-containercssclass=""><div class="col col-md-12"><!--Carousel-->
       
 <div id="myCarousel" class="carousel slide" data-ride="carousel" style = "height: 350px;">

	<ol class="carousel-indicators">
	<li data-target="#myCarousel" data-slide-to="0" class="active"></li>
	<li data-target="#myCarousel" data-slide-to="1"></li>
	<li data-target="#myCarousel" data-slide-to="2"></li>
	</ol>

	<div class="carousel-inner" style = "height: 350px;">
	<div class="item active" style="background-image:url('files/HTT.jpg'); background-size: cover;">
	<div class="container">
		<div class="carousel-caption" style="background-color: rgba(50,50,50,0.8); padding-left: .5%; padding-right:.5%; top: 5%; width: 75%; left:12.5%;">
			<h3>POLARBEAR: The first measurement of non-zero B-mode angular power spectrum</h3>
			<p>The POLARBEAR team recently measured the B-Mode polarization power spectrum at sub-degree scales.  </p>
			<p style="margin-top:5%;"><a class="btn btn-md btn-primary" href="newsfeed/31114" role="button">Read more</a></p>
		</div>
	</div>
	</div>
	<div class="item" style="background-image:url('files/9.jpg'); background-size: cover;">
		<div class="container">
		<div class="carousel-caption" style="background-color: rgba(50,50,50,0.8); padding-left: .5%; padding-right:.5%; top: 5%; width: 75%; left:12.5%;">
		<h3>GRAVITATIONAL LENSING of CMB POLARIZATION MEASURED </h3>
		<p> POLARBEAR measured gravitational lensing of the CMB using CMB polarization alone, and also showed ... </p>
		<p><a  class="btn btn-md btn-primary" href="newsfeed/12314" role="button">Read more</a></p>
		</div>
		</div>
	</div>
	<div class="item" style="background-image:url('files/6.jpg'); background-size: cover;">
		<div class="container">
		<div class="carousel-caption" style="background-color: rgba(50,50,50,0.8); padding-left: .5%; padding-right:.5%; top: 5%; width: 75%; left:12.5%;">
		<h3>YEAR AND A HALF AFTER FIRST LIGHT</h3>
		<p> A small update on what has happened since first light.</p>
		<p><a class="btn btn-md btn-primary" href="newsfeed/10313" role="button">Learn More</a></p>
		</div>
		</div>
	</div>
	</div>
	<a class="left carousel-control" href="#myCarousel" data-slide="prev"><span class="glyphicon glyphicon-chevron-left"></span></a>
	<a class="right carousel-control" href="#myCarousel" data-slide="next"><span class="glyphicon glyphicon-chevron-right"></span></a>

</div></div></div><div id="index-block-5" class="container"><div id="index-block-1" class="block row panel-body" data-nested="nested" data-containerid="index-block-5" data-containercssclass=""><div class="col col-md-9"><h3 id="index-h3-5"><?php print _("COSMIC MICROWAVE BACKGROUND POLARIZATION AND COSMOLOGY"); ?></h3><hr id="index-hr-5"></hr><p id="index-paragraph-1"><?php print _("Over the last two decades, studies of the <a href=\"http://en.wikipedia.org/wiki/Cosmic_microwave_background\">Cosmic Microwave Background (CMB)</a> intensity anisotropies have provided precise measurements of the basic parameters that describe our universe."); ?></p><p id="1396026969"><?php print _("The CMB polarization anisotropies offer a unique window onto cosmology and addresses some of the most mystifying questions of physics. POLARBEAR is a Cosmic Microwave Background polarization experiment. Our goal is to detect CMB <a href=\"http://en.wikipedia.org/wiki/B-modes\">B-modes</a> and use them to investigate the origin and evolution of the Universe and to understand physics and cosmology beyond our <a href=\"http://en.wikipedia.org/wiki/Lambda-CDM_model\">Standard Model</a>."); ?></p><p id="1396026969"><?php print _("Inflationary theories are currently the best framework we have to explain several aspects of the Universe today. They predict that the early Universe underwent a phase of exponential expansion during which a background of <a href=\"http://en.wikipedia.org/wiki/Gravitational_wave\">gravitational waves</a> was produced. Those gravitational waves will then produce a primordial B-mode signal at the time of recombination. An <a href=\"http://en.wikipedia.org/wiki/Inflation_(cosmology)\">inflationary phase</a> would have occurred at such high energy density that there is no hope of studying the phenomena in any accelerator that could be built on earth. POLARBEAR, however, will be able to see the direct signature of Inflation through CMB polarization, and will potentially be able to investigate physics that occurs at energies where all the forces of nature are unified."); ?></p><p id="1396026969"><?php print _("The evolution of the universe is based on the idea of gravitational instability, whereby initial tiny fluctuations in the density of the Universe grew under the influence of gravity to form the large-scale gravitational structures we see around us today. These structures bend the trajectories of CMB photons through gravitational lensing, distorting its primordial polarization and converting E-modes into B-modes. Imaging the lensing-generated B-modes, POLARBEAR will be able to shed light on all the components of the Universe influencing structure formation, such as neutrino mass and dark energy."); ?></p></div><div class="col col-md-3"><img src = "/respond/sites/polarbearv2/files/PB_in_atacma_with_CMB3.jpg" style = "height:500px;"/></div></div></div>
  
</div>
</div>
        
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/docs.min.js"></script>
</body>
</html>

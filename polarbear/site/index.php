<?php 
$rootPrefix="../";
$siteUniqId="5334aac3933db";
$siteFriendlyId="polarbearv2";
$pageUniqId="5339ad3168441";
$pageFriendlyId="index";
$pageTypeUniqId="5339abe629598";
$language="en";
include '../site.php';
?><!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="<?php print _("Measuring the CMB Polarization with POLARBEAR"); ?>">
	<meta name="author" content="Thomas Tu">
  <link rel="shortcut icon" href="/respond/sites/polarbearv2/files/acquia_slate_logo.png">

	<title>POLARBEAR</title>

  <link href="../css/carousel.css" rel="stylesheet">
    <link href="//netdna.bootstrapcdn.com/bootswatch/3.0.0/cerulean/bootstrap.min.css" rel="stylesheet">

	<!-- css -->
<link href="../css/content.css?v=2.8.3" type="text/css" rel="stylesheet" media="screen">
<link href="//cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.css" type="text/css" rel="stylesheet" media="screen">
<link href="//netdna.bootstrapcdn.com/font-awesome/4.0.2/css/font-awesome.css" rel="stylesheet">
<link href="../css/prettify.css?v=2.8.3" type="text/css" rel="stylesheet" media="screen">
</head>

<body data-siteuniqid="5334aac3933db" data-sitefriendlyid="polarbearv2" data-domain="bolo.berkeley.edu/polarbear" data-pageuniqid="5339ad3168441" data-pagefriendlyid="index" data-pagetypeuniqid="5339abe629598" data-api="localhost:8888/respond">

<!--Menu-->
<div class="navbar-wrapper">
<div class="container">

	<div class="navbar navbar-inverse navbar-static-top" role="navigation">
	<div class="container">
		<!--Make the menu mobile friendly-->
		<div class="navbar-header" >
		<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
		<span class="sr-only"></span>
		<span class="icon-bar"></span>
		<span class="icon-bar"></span>
		<span class="icon-bar"></span>
		</button>
          <a class="navbar-brand" href="//bolo.berkeley.edu/polarbear">POLARBEAR WITH THE HUAN TRAN TELESCOPE | Measuring Polarization of the CMB at the James Ax Observatory</a>
          </div>
	
	<div class="navbar-collapse collapse">
	 <ul class="nav navbar-nav" style="width:90%; margin-left: 5%;">
  		<li><?php print _("<a href=\"../././\">Home</a>"); ?></li><li><?php print _("<a href=\"../newsfeed\">Newsfeed</a>"); ?></li><li><?php print _("<a href=\"../technology\">Technology</a>"); ?></li><li><?php print _("<a href=\"../site\">Site</a>"); ?></li><li><?php print _("<a href=\"../future-plans\">Future Plans</a>"); ?></li><li><?php print _("<a href=\"../people\">People</a>"); ?></li><li><?php print _("<a href=\"../publications\">Publications</a>"); ?></li><li><?php print _("<a href=\"../support\">Support</a>"); ?></li><li><?php print _("<a href=\"../data\">Data</a>"); ?></li>
       </ul>
      </div>
	</div> 
	</div>
</div>
</div>
  
  

<div class="panel panel-default">
<div class="panel-body">
<div id="index-block-1" class="block row" data-nested="not-nested" data-containerid="" data-containercssclass=""><div class="col col-md-9"><h3 id="h3-2"><?php print _("LOCATION"); ?></h3><p id="index-paragraph-1"><?php print _("The POLARBEAR experiment is operated at the James Ax Observatory in the Chajnantor scientific reserve (Northern Chile) at an altitude of nearly 5200 m, which makes POLARBEAR one of the highest astronomical observatory in the world. Our colleagues from the <a href=\"http://www.physics.princeton.edu/act/\">ACT experiment</a> are our neighbours."); ?></p></div><div class="col col-md-3"><img src="/respond/sites/polarbearv2/files/cerro-chajnantor-map.jpg" style = "max-height: 300px; float: none; margin-top: 20px;"/></div></div><div id="block-2" class="block row" data-nested="not-nested" data-containerid="" data-containercssclass=""><div class="col col-md-3"><img src="/respond/sites/polarbearv2/files/HTT.jpg" style = "max-width: 200px; float: none; margin-top: 20px; margin-left:auto; margin-right:auto;"/></div><div class="col col-md-9"><h3 id="index-h3-1"><?php print _("WHY CHILE?"); ?></h3><p id="index-paragraph-2"><?php print _("Despite this site is highly inhospitable to humans, it is recognized as one of the best in the world for millimeter and submillimeter astronomy because of its dry climate which minimizes the absorbtion and attenuation of mm radiation by atmospheric water vapour."); ?></p></div></div><div id="block-3" class="block row" data-nested="not-nested" data-containerid="" data-containercssclass=""><div class="col col-md-12"><h3 id="h3-3"><?php print _("SITE OWNERSHIP"); ?></h3><img src="/respond/sites/polarbearv2/files/crop-pbsite-horizon.png" style = "max-height: 300px; float: none; margin-top: 20px;"/><p id="index-paragraph-3"><?php print _("The site belongs to the state of Chile and is leased to the collaboration. The site is maintained by CONICYT, Chile's National Comission for Scientific and Technological Research. The POLARBEAR experiment would not be possible without the collaboration and hard work of CONICYT."); ?></p></div></div>

</div>
</div>
        
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
  <script src="../js/bootstrap.min.js"></script>
  <script src="../js/docs.min.js"></script>
</body>
</html>

#!/usr/bin/env python

import os, cgi, requests, json, sys

links = {
	'pydfmux-chat/polarbearcmb': ['https://hooks.slack.com/services/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', 'https://conference.sptwiki.com/msg/pydfmux'],
	'pydfmux/southpoletelescope': ['https://hooks.slack.com/services/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', 'https://conference.sptwiki.com/msg/pydfmux'],
	'pydfmux/conference.sptwiki.com': ['https://hooks.slack.com/services/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', 'https://hooks.slack.com/services/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'],

	'ehtobs_rehearsals/eventhorizontelescope': ['https://conference.sptwiki.com/msg/ehtjan2017'],
	'ehtjan2017/conference.sptwiki.com': ['https://hooks.slack.com/services/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', "#ehtobs_rehearsals"],

	'eht2017/eventhorizontelescope': 'https://conference.sptwiki.com/msg/eht2017',
	'eht2017/conference.sptwiki.com': ('https://hooks.slack.com/services/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', '#eht2017'),

	'spt_support/eventhorizontelescope': 'https://conference.sptwiki.com/msg/spteht_support',
	'spteht_support/conference.sptwiki.com': ('https://hooks.slack.com/services/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', '#spt_support'),

	'ops/bicepkeck': 'https://conference.sptwiki.com/msg/bicep_ops',
	'bicep_ops/conference.sptwiki.com': 'https://hooks.slack.com/services/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',

	'readout-xtalk/polarbearcmb': ('https://hooks.slack.com/services/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', '#readout-xtalk'),
	'readout-xtalk/southpoletelescope': ('https://hooks.slack.com/services/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', '#readout-xtalk'),

	'winterover-monologues/conference.sptwiki.com': ('https://hooks.slack.com/services/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', '#winterover-monologues'),
	'winterover-monologues/southpoletelescope': 'https://conference.sptwiki.com/msg/winterover-monologues',

	'sptpipeline/conference.sptwiki.com': ('https://hooks.slack.com/services/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', '#sptpipeline'),
	'sptpipeline/southpoletelescope': 'https://conference.sptwiki.com/msg/sptpipeline',

	'sptpol/conference.sptwiki.com': ('https://hooks.slack.com/services/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', '#sptpol'),
	'sptpol/southpoletelescope': 'https://conference.sptwiki.com/msg/sptpol',

	'spt3g/conference.sptwiki.com': ('https://hooks.slack.com/services/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', '#spt3g'),
	'spt3g/southpoletelescope': 'https://conference.sptwiki.com/msg/spt3g',

	'spt/conference.sptwiki.com': ('https://hooks.slack.com/services/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', '#general'),
	'general/southpoletelescope': 'https://conference.sptwiki.com/msg/spt',

	'bkspt_delens/bicepkeck': ('https://hooks.slack.com/services/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', '#bkspt_delens'),
	'bkspt_delens/southpoletelescope': ('https://hooks.slack.com/services/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', '#bkspt_delens'),
}

# Avoid httpoxy issues
if 'HTTP_PROXY' in os.environ:
	del os.environ['HTTP_PROXY']

if os.environ['CONTENT_TYPE'] == 'application/x-www-form-urlencoded':
	data = cgi.parse()
else:
	rawdata = json.loads(sys.stdin.read(int(os.environ['CONTENT_LENGTH'])))
	data = {}
	for k in rawdata.keys():
		data[k] = [rawdata[k]]

if False:
	f = open('/tmp/cgilog', 'a')
	f.write(str(os.environ))
	f.write('\n')
	f.write(str(data))
	f.write('\n')
	f.close()


message = {'text': data['text'][0], 'username': data['user_name'][0]}

chatname = data['channel_name'][0] + '/' + data['team_domain'][0]
if chatname in links and 'bot_id' not in data:
	link = links[chatname]
	if not isinstance(link, list):
		link = [link]
	for l in link:
		if isinstance(l, tuple):
			url = l[0]
			message['channel'] = l[1]
		else:
			url = l
		requests.post(url, json=message, verify=False)

sys.stdout.write('Status: 200 OK\r\n\r\n')

if False:
	f = open('/tmp/cgilog', 'a')
	f.write('%s said "%s" on channel %s/%s\n' % (data['user_name'][0], data['text'][0], data['channel_name'][0], data['team_domain'][0]))
	f.close()

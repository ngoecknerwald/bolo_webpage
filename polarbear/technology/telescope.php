<?php 
$rootPrefix="../";
$siteUniqId="5334aac3933db";
$siteFriendlyId="polarbearv2";
$pageUniqId="5342ea13da2c6";
$pageFriendlyId="telescope";
$pageTypeUniqId="5335f99cd1e03";
$language="en";
include '../site.php';
?><!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="<?php print _("Measuring the CMB Polarization with POLARBEAR"); ?>">
	<meta name="author" content="Thomas Tu">
  <link rel="shortcut icon" href="/respond/sites/polarbearv2/files/acquia_slate_logo.png">

	<title>POLARBEAR</title>

  <link href="../css/carousel.css" rel="stylesheet">
    <link href="//netdna.bootstrapcdn.com/bootswatch/3.0.0/cerulean/bootstrap.min.css" rel="stylesheet">

	<!-- css -->
<link href="../css/content.css?v=2.8.3" type="text/css" rel="stylesheet" media="screen">
<link href="//cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.css" type="text/css" rel="stylesheet" media="screen">
<link href="//netdna.bootstrapcdn.com/font-awesome/4.0.2/css/font-awesome.css" rel="stylesheet">
<link href="../css/prettify.css?v=2.8.3" type="text/css" rel="stylesheet" media="screen">
</head>

<body data-siteuniqid="5334aac3933db" data-sitefriendlyid="polarbearv2" data-domain="bolo.berkeley.edu/polarbear" data-pageuniqid="5342ea13da2c6" data-pagefriendlyid="telescope" data-pagetypeuniqid="5335f99cd1e03" data-api="localhost:8888/respond">

<!--Menu-->
<div class="navbar-wrapper">
<div class="container">

	<div class="navbar navbar-inverse navbar-static-top" role="navigation">
	<div class="container">
		<!--Make the menu mobile friendly-->
		<div class="navbar-header" >
		<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
		<span class="sr-only"></span>
		<span class="icon-bar"></span>
		<span class="icon-bar"></span>
		<span class="icon-bar"></span>
		</button>
          <a class="navbar-brand" href="//bolo.berkeley.edu/polarbear">POLARBEAR WITH THE HUAN TRAN TELESCOPE | Measuring Polarization of the CMB at the James Ax Observatory</a>
          </div>
	
	<div class="navbar-collapse collapse">
	 <ul class="nav navbar-nav" style="width:90%; margin-left: 5%;">
  		<li><?php print _("<a href=\"../././\">Home</a>"); ?></li><li><?php print _("<a href=\"../newsfeed\">Newsfeed</a>"); ?></li><li><?php print _("<a href=\"../technology\">Technology</a>"); ?></li><li><?php print _("<a href=\"../site\">Site</a>"); ?></li><li><?php print _("<a href=\"../future-plans\">Future Plans</a>"); ?></li><li><?php print _("<a href=\"../people\">People</a>"); ?></li><li><?php print _("<a href=\"../publications\">Publications</a>"); ?></li><li><?php print _("<a href=\"../support\">Support</a>"); ?></li><li><?php print _("<a href=\"../data\">Data</a>"); ?></li>
       </ul>
      </div>
	</div> 
	</div>
</div>
</div>
  
  

<div class="panel panel-default">
<div class="panel-body">
<div id="readout-block-1" class="block row" data-nested="not-nested" data-containerid="" data-containercssclass=""><div class="col col-md-3"><p id="readout-paragraph-5"><?php print _("<i class=\"fa fa-chevron-left\"></i> <a href=\"./\">Back to technology page</a>"); ?></p></div><div class="col col-md-9"><h3 id="readout-h3-1"><?php print _("OVERVIEW"); ?></h3><p id="readout-paragraph-1"><?php print _("The Huan Tran Telescope, shown below, was designed to have a high throughout, low cross polarization, and low sidelobe response. It was also designed to have 3.5 arcminute beam size at 150 GHz - small enough to resolve the lensing signal at multipole l~1000. The huan tran telescope is composed of (a) 2.5 meter precision-machined monolithic primary (b) guard ring extending to 3.5 meters (c) Co-moving ground shield (d) Pirme focus baffle"); ?></p><img src="http://cosmology.ucsd.edu/~thomastu/polarbeartest/sites/default/files/Technology/Telescope/Telescope.jpg" style="max-height: 300px;"/><h3 id="readout-h3-2"><?php print _("DESIGN NOTES"); ?></h3><p id="readout-paragraph-2"><?php print _("The Polarbear telescope was custom designed and fabricated by General Dynamics SATCOM. Polabear is a microwave telescope, and looks most like a radio telescope or satellite dish. The Polarbear telescope is similar to other radio telescopes, but unlike previous telescopes, Polarbear was designed for a very large Field of View, to accomidate the large imaging camera. This large Field of VIew is what ultimtely allows us to see the faintest signals from the early universe. The unusual shape of the telescope was chosen for optical purity. The mirrors are in an offset configuration, allowing microwaves from the sky to reach the camera without having to go around an obstruction. With a direct path to the camera, there is less of a chance for microwaves to be scattered into unwanted directions. Once the light reached the camera, lenses inside the camera refocus an image of the sky onto the imaging array."); ?></p><h3 id="telescope-h3-4"><?php print _("ABOUT HUAN TRAN"); ?></h3><p id="telescope-paragraph-4"><?php print _("In December 2009, the POLARBEAR project manager Huan Tran died in a tragic domestic accident while on a trip to work on the telescope. We are all greatly saddened. We lost a great leader, scientist, and friend. Huan was involved in every design aspect of the experiment and was deeply dedicated to the project. The telescope has now been renamed the Huan Tran Telescope (HTT) in his honor."); ?></p><center><img src="/respond/sites/polarbearv2/files/huantran.jpg" style= "max-height: 300px; margin-left: auto;"/></center><p id="telescope-paragraph-5" class="card    align-left"><?php print _("We miss him greatly."); ?></p><center><img src="/respond/sites/polarbearv2/files/memorial.jpg" style= "max-height: 300px; margin-left: auto;"/></center></div></div>

</div>
</div>
        
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
  <script src="../js/bootstrap.min.js"></script>
  <script src="../js/docs.min.js"></script>
</body>
</html>

<?php 
$rootPrefix="../";
$siteUniqId="5334aac3933db";
$siteFriendlyId="polarbearv2";
$pageUniqId="533e2e68518d2";
$pageFriendlyId="index";
$pageTypeUniqId="533e2e398fae0";
$language="en";
include '../site.php';
?><!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="<?php print _("Measuring the CMB Polarization with POLARBEAR"); ?>">
	<meta name="author" content="Thomas Tu">
  <link rel="shortcut icon" href="/respond/sites/polarbearv2/files/acquia_slate_logo.png">

	<title>POLARBEAR</title>

  <link href="../css/carousel.css" rel="stylesheet">
    <link href="//netdna.bootstrapcdn.com/bootswatch/3.0.0/cerulean/bootstrap.min.css" rel="stylesheet">

	<!-- css -->
<link href="../css/content.css?v=2.8.3" type="text/css" rel="stylesheet" media="screen">
<link href="//cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.css" type="text/css" rel="stylesheet" media="screen">
<link href="//netdna.bootstrapcdn.com/font-awesome/4.0.2/css/font-awesome.css" rel="stylesheet">
<link href="../css/prettify.css?v=2.8.3" type="text/css" rel="stylesheet" media="screen">
</head>

<body data-siteuniqid="5334aac3933db" data-sitefriendlyid="polarbearv2" data-domain="bolo.berkeley.edu/polarbear" data-pageuniqid="533e2e68518d2" data-pagefriendlyid="index" data-pagetypeuniqid="533e2e398fae0" data-api="localhost:8888/respond">

<!--Menu-->
<div class="navbar-wrapper">
<div class="container">

	<div class="navbar navbar-inverse navbar-static-top" role="navigation">
	<div class="container">
		<!--Make the menu mobile friendly-->
		<div class="navbar-header" >
		<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
		<span class="sr-only"></span>
		<span class="icon-bar"></span>
		<span class="icon-bar"></span>
		<span class="icon-bar"></span>
		</button>
          <a class="navbar-brand" href="//bolo.berkeley.edu/polarbear">POLARBEAR WITH THE HUAN TRAN TELESCOPE | Measuring Polarization of the CMB at the James Ax Observatory</a>
          </div>
	
	<div class="navbar-collapse collapse">
	 <ul class="nav navbar-nav" style="width:90%; margin-left: 5%;">
  		<li><?php print _("<a href=\"../././\">Home</a>"); ?></li><li><?php print _("<a href=\"../newsfeed\">Newsfeed</a>"); ?></li><li><?php print _("<a href=\"../technology\">Technology</a>"); ?></li><li><?php print _("<a href=\"../site\">Site</a>"); ?></li><li><?php print _("<a href=\"../future-plans\">Future Plans</a>"); ?></li><li><?php print _("<a href=\"../people\">People</a>"); ?></li><li><?php print _("<a href=\"../publications\">Publications</a>"); ?></li><li><?php print _("<a href=\"../support\">Support</a>"); ?></li><li><?php print _("<a href=\"../data\">Data</a>"); ?></li>
       </ul>
      </div>
	</div> 
	</div>
</div>
</div>
  
  

<div class="panel panel-default">
<div class="panel-body">
<div id="index-block-6" class="block row newscard" data-nested="not-nested" data-containerid="" data-containercssclass=""><div class="col col-md-9"><h3 id="index-h3-6"><?php print _("<a href=\"31114\">POLARBEAR MEASURES the B-MODE POLARIZATION POWER SPECTRUM</a>"); ?></h3><p id="index-paragraph-6"><?php print _("The POLARBEAR team produced the first direct measurement of the cosmic polarization induced by structure in the universe. Dark matter twists the polarization into a 'B-mode' signal at arc minute scales on the sky. The measurement rejects the possibility of zero B-mode power from gravitational lensing at 97.5% confidence (<a href=\"http://arxiv.org/pdf/1403.2369v1.pdf\">arxiv:1403.2369</a>). This measurement was produced by the first year of POLARBEAR observations. This announcement came one week before the BICEP2 team announced their detection of B-mode power at degree scales, consistent with an energy scale for cosmic inflation of about 1016 GeV. This was an exciting week for Cosmology! The figure below shows the BICEP2 and POLARBEAR measurements."); ?></p></div><div class="col col-md-3"><a href="http://arxiv.org/pdf/1403.2369v1.pdf"target= "_blank" ><img src="/respond/sites/polarbearv2/files/BB_science.png" style="height:100px; margin-left:auto; margin-right:auto; margin-top: 10px;"/></a></div></div><div id="index-block-4" class="block row newscard" data-nested="not-nested" data-containerid="" data-containercssclass=""><div class="col col-md-9"><h3 id="index-h3-5"><?php print _("<a href=\"../newsfeed/12314\">GRAVITATIONAL LENSING of B-MODE POLARIZATION MEASURED w/ POLARBEAR</a>"); ?></h3><p id="index-paragraph-5"><?php print _("Cosmological results from the first season of POLARBEAR observations have been submitted for publication. We measured gravitational lensing of the CMB using CMB polarization alone (arxiv.org/1312.6646), and also showed that the lensing convergence map correlates with the large-scale structure traced by Herschel 500 micron cosmic infrared background observations (arxiv.org/1312.6645)"); ?></p></div><div class="col col-md-3"><img src="http://cosmology.ucsd.edu/~thomastu/Polarbeartest/sites/default/files/Stories/FirstPaper/paper1" style="height: 100px;"/></div></div><div id="block-5" class="block row newscard" data-nested="not-nested" data-containerid="" data-containercssclass=""><div class="col col-md-9"><h3 id="index-h3-4"><?php print _("<a href=\"../newsfeed/10313\">YEAR AND A HALF AFTER FIRST LIGHT</a>"); ?></h3><p id="index-paragraph-4"><?php print _("The POLARBEAR team is busy analyzing cosmological data while observations continue. Below we see a map of detected patterns in E-Mode polarization in the Chilean sky. Understanding these patterns will hopefully shed light on the behavior of the early universe."); ?></p></div><div class="col col-md-3"><img src="http://cosmology.ucsd.edu/~thomastu/Polarbeartest/sites/default/files/FirstYearAnalysis/HTT.jpg" style="height: 100px;"/></div></div><div id="index-block-3" class="block row newscard" data-nested="not-nested" data-containerid="" data-containercssclass=""><div class="col col-md-9"><h3 id="index-h3-3"><?php print _("<a href=\"../newsfeed/22312\">FIRST LIGHT IN CHILE</a>"); ?></h3><p id="index-paragraph-3"><?php print _("The POLARBEAR experiment achieved first light in Chile on January 10th, 2012 with observations of Saturn. Below we show signals from two polarization-sensitive detectors in one array pixel. To the right, the 1284-bolometer array is being installed. At the bottom, a beam map from Jupiter is shown. The team is currently preparing the system for science observations in April, when weather conditions in Chile become suitable."); ?></p></div><div class="col col-md-3"></div></div><div id="index-block-2" class="block row newscard" data-nested="not-nested" data-containerid="" data-containercssclass=""><div class="col col-md-9"><h3 id="index-h3-2"><?php print _("<a href=\"../newsfeed/83110\">ENGINEERING RUN IS COMPLETE</a>"); ?></h3><p id="index-paragraph-2"><?php print _("Engineering run has been successfully completed at Cedar Flat, CA! Next stop: Atacama desert!"); ?></p></div><div class="col col-md-3"></div></div><div id="block-2" class="block row newscard" data-nested="not-nested" data-containerid="" data-containercssclass=""><div class="col col-md-9"><h3 id="index-h3-1"><?php print _("<a href=\"../newsfeed/10609\">POLARBEAR INSTALLATION AT CEDAR FLATS</a>"); ?></h3><p id="index-paragraph-1"><?php print _("The Polarbear telescope installation is now complete! The installers from GD-SATCOM have signed off on a working telescope. Polarbear is currently situated at PAD 25 amidst the CARMA array."); ?></p></div><div class="col col-md-3"><img src="../files/PBT_cedar_flat.jpg" style="height:100px; margin-top: 10%; margin-bottom: 10%;"/></div></div>

</div>
</div>
        
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
  <script src="../js/bootstrap.min.js"></script>
  <script src="../js/docs.min.js"></script>
</body>
</html>

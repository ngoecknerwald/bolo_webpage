<?php 
$rootPrefix="../";
$siteUniqId="5334aac3933db";
$siteFriendlyId="polarbearv2";
$pageUniqId="533e34175b39f";
$pageFriendlyId="10313";
$pageTypeUniqId="533e2e398fae0";
$language="en";
include '../site.php';
?><!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="<?php print _("Measuring the CMB Polarization with POLARBEAR"); ?>">
	<meta name="author" content="Thomas Tu">
  <link rel="shortcut icon" href="/respond/sites/polarbearv2/files/acquia_slate_logo.png">

	<title>POLARBEAR</title>

  <link href="../css/carousel.css" rel="stylesheet">
    <link href="//netdna.bootstrapcdn.com/bootswatch/3.0.0/cerulean/bootstrap.min.css" rel="stylesheet">

	<!-- css -->
<link href="../css/content.css?v=2.8.3" type="text/css" rel="stylesheet" media="screen">
<link href="//cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.css" type="text/css" rel="stylesheet" media="screen">
<link href="//netdna.bootstrapcdn.com/font-awesome/4.0.2/css/font-awesome.css" rel="stylesheet">
<link href="../css/prettify.css?v=2.8.3" type="text/css" rel="stylesheet" media="screen">
</head>

<body data-siteuniqid="5334aac3933db" data-sitefriendlyid="polarbearv2" data-domain="bolo.berkeley.edu/polarbear" data-pageuniqid="533e34175b39f" data-pagefriendlyid="10313" data-pagetypeuniqid="533e2e398fae0" data-api="localhost:8888/respond">

<!--Menu-->
<div class="navbar-wrapper">
<div class="container">

	<div class="navbar navbar-inverse navbar-static-top" role="navigation">
	<div class="container">
		<!--Make the menu mobile friendly-->
		<div class="navbar-header" >
		<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
		<span class="sr-only"></span>
		<span class="icon-bar"></span>
		<span class="icon-bar"></span>
		<span class="icon-bar"></span>
		</button>
          <a class="navbar-brand" href="//bolo.berkeley.edu/polarbear">POLARBEAR WITH THE HUAN TRAN TELESCOPE | Measuring Polarization of the CMB at the James Ax Observatory</a>
          </div>
	
	<div class="navbar-collapse collapse">
	 <ul class="nav navbar-nav" style="width:90%; margin-left: 5%;">
  		<li><?php print _("<a href=\"../././\">Home</a>"); ?></li><li><?php print _("<a href=\"../newsfeed\">Newsfeed</a>"); ?></li><li><?php print _("<a href=\"../technology\">Technology</a>"); ?></li><li><?php print _("<a href=\"../site\">Site</a>"); ?></li><li><?php print _("<a href=\"../future-plans\">Future Plans</a>"); ?></li><li><?php print _("<a href=\"../people\">People</a>"); ?></li><li><?php print _("<a href=\"../publications\">Publications</a>"); ?></li><li><?php print _("<a href=\"../support\">Support</a>"); ?></li><li><?php print _("<a href=\"../data\">Data</a>"); ?></li>
       </ul>
      </div>
	</div> 
	</div>
</div>
</div>
  
  

<div class="panel panel-default">
<div class="panel-body">
<div id="100609-block-2" class="block row" data-nested="not-nested" data-containerid="" data-containercssclass=""><div class="col col-md-3"><p id="100609-paragraph-1"><?php print _("<i class=\"fa fa-chevron-left\"></i> <a href=\"../newsfeed/index\">Go back to newsfeed</a>"); ?></p></div><div class="col col-md-9"><h3 id="100609-h3-2"><?php print _("FIRST LIGHT IN CHILE"); ?></h3><p id="100609-paragraph-2"><?php print _("The POLARBEAR experiment achieved first light in Chile on January 10th, 2012 with observations of Saturn. Below we show signals from two polarization-sensitive detectors in one array pixel. Below, the 1284-bolometer array is being installed. At the bottom, a beam map from Jupiter is shown. The team is currently preparing the system for science observations in April, when weather conditions in Chile become suitable."); ?></p><img src="http://cosmology.ucsd.edu/~thomastu/Polarbeartest/sites/default/files/FirstYearAnalysis/E_map2.png" style = "height: 300px; margin-left: 10%;" />

<img src="http://cosmology.ucsd.edu/~thomastu/Polarbeartest/sites/default/files/FirstYearAnalysis/E_null_map2.png" style = "height: 300px; margin-left: 5px;" /><p id="10313-paragraph-3" class="card"><?php print _("(a) The map on the left reveals a clear detection of E-mode polarization in the CMB. E-mode polarization has already been measured by several experiments. The source of this polarization is the acoustic oscillations of the early universe. (b) Oon the left we have a \"null map\"  which shows a map with the data split in half and differenced, so that only noise should remain. This low noise level will allow us to also characterize the B-mode polarization."); ?></p><img src="http://cosmology.ucsd.edu/~thomastu/Polarbeartest/sites/default/files/FirstYearAnalysis/tau_A.jpg" style = "height: 300px; margin-left: 25%; margin-top: 2.5%;"/><p id="10313-paragraph-4" class="card"><?php print _("(c) We use the polarized supernova remnant Tau A (also known as the Crab Nebula) as a polarization calibrator for the instrument. This is a map of the polarized intensity and polarization direction of Tau A, produced from data taken over the past 1.5 years."); ?></p></div></div>

</div>
</div>
        
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
  <script src="../js/bootstrap.min.js"></script>
  <script src="../js/docs.min.js"></script>
</body>
</html>

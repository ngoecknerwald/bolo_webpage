<?php 
$rootPrefix="../";
$siteUniqId="5334aac3933db";
$siteFriendlyId="polarbearv2";
$pageUniqId="5339adbb90b1d";
$pageFriendlyId="index";
$pageTypeUniqId="5339acca75e9a";
$language="en";
include '../site.php';
?><!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="<?php print _("Measuring the CMB Polarization with POLARBEAR"); ?>">
	<meta name="author" content="Thomas Tu">
  <link rel="shortcut icon" href="/respond/sites/polarbearv2/files/acquia_slate_logo.png">

	<title>POLARBEAR</title>

  <link href="../css/carousel.css" rel="stylesheet">
    <link href="//netdna.bootstrapcdn.com/bootswatch/3.0.0/cerulean/bootstrap.min.css" rel="stylesheet">

	<!-- css -->
<link href="../css/content.css?v=2.8.3" type="text/css" rel="stylesheet" media="screen">
<link href="//cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.css" type="text/css" rel="stylesheet" media="screen">
<link href="//netdna.bootstrapcdn.com/font-awesome/4.0.2/css/font-awesome.css" rel="stylesheet">
<link href="../css/prettify.css?v=2.8.3" type="text/css" rel="stylesheet" media="screen">
</head>

<body data-siteuniqid="5334aac3933db" data-sitefriendlyid="polarbearv2" data-domain="bolo.berkeley.edu/polarbear" data-pageuniqid="5339adbb90b1d" data-pagefriendlyid="index" data-pagetypeuniqid="5339acca75e9a" data-api="localhost:8888/respond">

<!--Menu-->
<div class="navbar-wrapper">
<div class="container">

	<div class="navbar navbar-inverse navbar-static-top" role="navigation">
	<div class="container">
		<!--Make the menu mobile friendly-->
		<div class="navbar-header" >
		<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
		<span class="sr-only"></span>
		<span class="icon-bar"></span>
		<span class="icon-bar"></span>
		<span class="icon-bar"></span>
		</button>
          <a class="navbar-brand" href="//bolo.berkeley.edu/polarbear">POLARBEAR WITH THE HUAN TRAN TELESCOPE | Measuring Polarization of the CMB at the James Ax Observatory</a>
          </div>
	
	<div class="navbar-collapse collapse">
	 <ul class="nav navbar-nav" style="width:90%; margin-left: 5%;">
  		<li><?php print _("<a href=\"../././\">Home</a>"); ?></li><li><?php print _("<a href=\"../newsfeed\">Newsfeed</a>"); ?></li><li><?php print _("<a href=\"../technology\">Technology</a>"); ?></li><li><?php print _("<a href=\"../site\">Site</a>"); ?></li><li><?php print _("<a href=\"../future-plans\">Future Plans</a>"); ?></li><li><?php print _("<a href=\"../people\">People</a>"); ?></li><li><?php print _("<a href=\"../publications\">Publications</a>"); ?></li><li><?php print _("<a href=\"../support\">Support</a>"); ?></li><li><?php print _("<a href=\"../data\">Data</a>"); ?></li>
       </ul>
      </div>
	</div> 
	</div>
</div>
</div>
  
  

<div class="panel panel-default">
<div class="panel-body">
<div id="intro" class="block row" data-nested="not-nested" data-containerid="" data-containercssclass=""><div class="col col-md-12"><p id="index-paragraph-1" class="align-center"><?php print _("The POLARBEAR experiment is made possible through the support of the institutions below. We give them our greatest gratitude for their dedication pursuant toward furthering our understanding of the cosmos."); ?></p></div></div><div id="NSF" class="block row" data-nested="not-nested" data-containerid="" data-containercssclass=""><div class="col col-md-3"></div><div class="col col-md-9"><h3 id="index-h3-2" class="smallheading"><?php print _("<a href=\"http://www.nsf.gov/\">NATIONAL SCIENCE FOUNDATION</a>"); ?></h3><p id="index-paragraph-2"><?php print _("The NSF is a federal agency created \"to promote the progress of science, advance the national health, prosperity and welfare; [and] to secure the national defense\""); ?></p></div></div><div id="block-3" class="block row" data-nested="not-nested" data-containerid="" data-containercssclass=""><div class="col col-md-3"></div><div class="col col-md-9"><h3 id="index-h3-3" class="smallheading"><?php print _("JAMES B AX FAMILY FOUNDATION"); ?></h3><p id="index-paragraph-3"><?php print _("The James B Ax Family Foundation is a private non-profit which focuses on private grantmaking programs."); ?></p></div></div><div id="block-4" class="block row" data-nested="not-nested" data-containerid="" data-containercssclass=""><div class="col col-md-3"></div><div class="col col-md-9"><h3 id="index-h3-4" class="smallheading"><?php print _("<a href=\"http://www.mext.go.jp/english/\">MEXT</a>"); ?></h3><p id="index-paragraph-4"><?php print _("The Ministry of Education, Culture, Sports, Science and Technology, or MEXT is a ministry of the Japanese government which supports various programs which facilitate research exchanges and joint research with nations abroad."); ?></p></div></div><div id="block-5" class="block row" data-nested="not-nested" data-containerid="" data-containercssclass=""><div class="col col-md-3"></div><div class="col col-md-9"><h3 id="index-h3-5" class="smallheading"><?php print _("<a href=\"https://www.simonsfoundation.org/\">SIMONS FOUNDATION</a>"); ?></h3><p id="index-paragraph-5"><?php print _("The Simons Foundation’s mission is to advance the frontiers of research in mathematics and the basic sciences. They sponsor a range of programs that aim to promote a deeper understanding of our world.<br>"); ?></p></div></div><div id="block-6" class="block row" data-nested="not-nested" data-containerid="" data-containercssclass=""><div class="col col-md-3"></div><div class="col col-md-9"><h3 id="index-h3-6" class="smallheading"><?php print _("<a href=\"http://www.nsf.gov/\">DEPARTMENT OF ENERGY, OFFICE OF HIGH ENERGY PHYSICS</a>"); ?></h3><p id="index-paragraph-6"><?php print _("The office of high energy physics sets out to understand how our universe works at its most fundamental level. HEP supports theoretical and experimental research in both elementary particle physics and fundamental accelerator science and technology."); ?></p></div></div><div id="block-7" class="block row" data-nested="not-nested" data-containerid="" data-containercssclass=""><div class="col col-md-3"></div><div class="col col-md-9"><h3 id="index-h3-7" class="smallheading"><?php print _("<a href=\"http://www.nserc-crsng.gc.ca/index_eng.asp\">NATURAL SCIENCES AND ENGINEERING RESEARCH COUNCIL OF CANADA</a>"); ?></h3><p id="index-paragraph-7"><?php print _("The Natural Sciences and Engineering Research Council of Canada is a Canadian government agency that promotes discovery by funding research."); ?></p></div></div><div id="block-8" class="block row" data-nested="not-nested" data-containerid="" data-containercssclass=""><div class="col col-md-3"></div><div class="col col-md-9"><h3 id="index-h3-8" class="smallheading"><?php print _("<a href=\"http://ec.europa.eu/research/mariecurieactions/\">SCIENCE AND TECHNOLOGY FACILITIES COUNCIL</a>"); ?></h3><p id="index-paragraph-8"><?php print _("The STFC is one of the UK’s seven publicly funded Research Councils responsible for supporting, co-ordinating and promoting research, innovation and skills development in seven distinct fields."); ?></p></div></div><div id="block-9" class="block row" data-nested="not-nested" data-containerid="" data-containercssclass=""><div class="col col-md-3"></div><div class="col col-md-9"><h3 id="index-h3-9" class="smallheading"><?php print _("<a href=\"http://www.conicyt.cl/\">NATIONAL COMMISSION FOR SCIENTIFIC AND TECHNOLOGICAL RESEARCH</a>"); ?></h3><p id="index-paragraph-9"><?php print _("Chile's National Comission for Scientific and Technological Research was established in 1967 as an advisory body to the President of the Republic of Chile on scientific matters. Today, the body focuses on promoting, developing and disseminating scientific and technological research."); ?></p></div></div>

</div>
</div>
        
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
  <script src="../js/bootstrap.min.js"></script>
  <script src="../js/docs.min.js"></script>
</body>
</html>

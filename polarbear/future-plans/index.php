<?php 
$rootPrefix="../";
$siteUniqId="5334aac3933db";
$siteFriendlyId="polarbearv2";
$pageUniqId="5339ada69d4d4";
$pageFriendlyId="index";
$pageTypeUniqId="5339abfbac5f5";
$language="en";
include '../site.php';
?><!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="<?php print _("Measuring the CMB Polarization with POLARBEAR"); ?>">
	<meta name="author" content="Thomas Tu">
  <link rel="shortcut icon" href="/respond/sites/polarbearv2/files/acquia_slate_logo.png">

	<title>POLARBEAR</title>

  <link href="../css/carousel.css" rel="stylesheet">
    <link href="//netdna.bootstrapcdn.com/bootswatch/3.0.0/cerulean/bootstrap.min.css" rel="stylesheet">

	<!-- css -->
<link href="../css/content.css?v=2.8.3" type="text/css" rel="stylesheet" media="screen">
<link href="//cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.css" type="text/css" rel="stylesheet" media="screen">
<link href="//netdna.bootstrapcdn.com/font-awesome/4.0.2/css/font-awesome.css" rel="stylesheet">
<link href="../css/prettify.css?v=2.8.3" type="text/css" rel="stylesheet" media="screen">
</head>

<body data-siteuniqid="5334aac3933db" data-sitefriendlyid="polarbearv2" data-domain="bolo.berkeley.edu/polarbear" data-pageuniqid="5339ada69d4d4" data-pagefriendlyid="index" data-pagetypeuniqid="5339abfbac5f5" data-api="localhost:8888/respond">

<!--Menu-->
<div class="navbar-wrapper">
<div class="container">

	<div class="navbar navbar-inverse navbar-static-top" role="navigation">
	<div class="container">
		<!--Make the menu mobile friendly-->
		<div class="navbar-header" >
		<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
		<span class="sr-only"></span>
		<span class="icon-bar"></span>
		<span class="icon-bar"></span>
		<span class="icon-bar"></span>
		</button>
          <a class="navbar-brand" href="//bolo.berkeley.edu/polarbear">POLARBEAR WITH THE HUAN TRAN TELESCOPE | Measuring Polarization of the CMB at the James Ax Observatory</a>
          </div>
	
	<div class="navbar-collapse collapse">
	 <ul class="nav navbar-nav" style="width:90%; margin-left: 5%;">
  		<li><?php print _("<a href=\"../././\">Home</a>"); ?></li><li><?php print _("<a href=\"../newsfeed\">Newsfeed</a>"); ?></li><li><?php print _("<a href=\"../technology\">Technology</a>"); ?></li><li><?php print _("<a href=\"../site\">Site</a>"); ?></li><li><?php print _("<a href=\"../future-plans\">Future Plans</a>"); ?></li><li><?php print _("<a href=\"../people\">People</a>"); ?></li><li><?php print _("<a href=\"../publications\">Publications</a>"); ?></li><li><?php print _("<a href=\"../support\">Support</a>"); ?></li><li><?php print _("<a href=\"../data\">Data</a>"); ?></li>
       </ul>
      </div>
	</div> 
	</div>
</div>
</div>
  
  

<div class="panel panel-default">
<div class="panel-body">
<div id="index-block-1" class="block row" data-nested="not-nested" data-containerid="" data-containercssclass=""><div class="col col-md-9"><h3 id="index-h3-1"><?php print _("POLARBEAR 2"); ?></h3><p id="index-paragraph-2"><?php print _("The next-generation receiver, scheduled for first light in 2015, will use a new dichroic pixel architecture with bands at 95 and 150 GHz. The focal plane will be larger in area and pixel count than POLARBEAR-1, with 1897 pixels (7588 bolometers). The receiver shown to the right currently under construction."); ?></p></div><div class="col col-md-3"><img src="http://cosmology.ucsd.edu/~thomastu/polarbeartest/sites/default/files/FuturePlans/Receiver.jpg" style = "min-width: 150px; width: 75%; margin-top: 20px;"/></div></div><div id="block-2" class="block row" data-nested="not-nested" data-containerid="" data-containercssclass=""><div class="col col-md-3"><img src="http://cosmology.ucsd.edu/~thomastu/polarbeartest/sites/default/files/FuturePlans/SimonsArray.jpg" style = "float: right; min-width: 150px; width: 75%; margin-top: 20px;"/></div><div class="col col-md-9"><h3 id="h3-2"><?php print _("SIMONS ARRAY"); ?></h3><p id="index-paragraph-1"><?php print _("The Simons Array is a funded expansion of POLARBEAR to three multi-frequency telescopes based on the POLARBEAR-2 design at the Cerro Toco site. The sensitivity and multi-frequency observation will allow us to precisely characterize the inflationary and lensing B-mode signals. From the lensing signal we will be able to measure the sum of the neutrino masses with an uncertainty of 16 meV (when combined with BAO data), and constrain the equation of state of dark energy. We will also be able to remove the lensing, treating it as a \"foreground  contaminant\" to measure the primordial inflationary B-mode signal across a wide multipole range."); ?></p></div></div>

</div>
</div>
        
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
  <script src="../js/bootstrap.min.js"></script>
  <script src="../js/docs.min.js"></script>
</body>
</html>

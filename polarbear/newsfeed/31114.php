<?php 
$rootPrefix="../";
$siteUniqId="5334aac3933db";
$siteFriendlyId="polarbearv2";
$pageUniqId="533e39a21f538";
$pageFriendlyId="31114";
$pageTypeUniqId="533e2e398fae0";
$language="en";
include '../site.php';
?><!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="<?php print _("Measuring the CMB Polarization with POLARBEAR"); ?>">
	<meta name="author" content="Thomas Tu">
  <link rel="shortcut icon" href="/respond/sites/polarbearv2/files/acquia_slate_logo.png">

	<title>POLARBEAR</title>

  <link href="../css/carousel.css" rel="stylesheet">
    <link href="//netdna.bootstrapcdn.com/bootswatch/3.0.0/cerulean/bootstrap.min.css" rel="stylesheet">

	<!-- css -->
<link href="../css/content.css?v=2.8.3" type="text/css" rel="stylesheet" media="screen">
<link href="//cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.css" type="text/css" rel="stylesheet" media="screen">
<link href="//netdna.bootstrapcdn.com/font-awesome/4.0.2/css/font-awesome.css" rel="stylesheet">
<link href="../css/prettify.css?v=2.8.3" type="text/css" rel="stylesheet" media="screen">
</head>

<body data-siteuniqid="5334aac3933db" data-sitefriendlyid="polarbearv2" data-domain="bolo.berkeley.edu/polarbear" data-pageuniqid="533e39a21f538" data-pagefriendlyid="31114" data-pagetypeuniqid="533e2e398fae0" data-api="localhost:8888/respond">

<!--Menu-->
<div class="navbar-wrapper">
<div class="container">

	<div class="navbar navbar-inverse navbar-static-top" role="navigation">
	<div class="container">
		<!--Make the menu mobile friendly-->
		<div class="navbar-header" >
		<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
		<span class="sr-only"></span>
		<span class="icon-bar"></span>
		<span class="icon-bar"></span>
		<span class="icon-bar"></span>
		</button>
          <a class="navbar-brand" href="//bolo.berkeley.edu/polarbear">POLARBEAR WITH THE HUAN TRAN TELESCOPE | Measuring Polarization of the CMB at the James Ax Observatory</a>
          </div>
	
	<div class="navbar-collapse collapse">
	 <ul class="nav navbar-nav" style="width:90%; margin-left: 5%;">
  		<li><?php print _("<a href=\"../././\">Home</a>"); ?></li><li><?php print _("<a href=\"../newsfeed\">Newsfeed</a>"); ?></li><li><?php print _("<a href=\"../technology\">Technology</a>"); ?></li><li><?php print _("<a href=\"../site\">Site</a>"); ?></li><li><?php print _("<a href=\"../future-plans\">Future Plans</a>"); ?></li><li><?php print _("<a href=\"../people\">People</a>"); ?></li><li><?php print _("<a href=\"../publications\">Publications</a>"); ?></li><li><?php print _("<a href=\"../support\">Support</a>"); ?></li><li><?php print _("<a href=\"../data\">Data</a>"); ?></li>
       </ul>
      </div>
	</div> 
	</div>
</div>
</div>
  
  

<div class="panel panel-default">
<div class="panel-body">
<div id="100609-block-2" class="block row" data-nested="not-nested" data-containerid="" data-containercssclass=""><div class="col col-md-3"><p id="100609-paragraph-1"><?php print _("<i class=\"fa fa-chevron-left\"></i> <a href=\"../newsfeed/index\">Go back to newsfeed</a>"); ?></p></div><div class="col col-md-9"><h3 id="100609-h3-2"><?php print _("POLARBEAR: The first measurement of non-zero B-mode angular power spectrum"); ?></h3><p id="100609-paragraph-2"><?php print _("The POLARBEAR team produced the first direct measurement of the cosmic polarization induced by structure in the universe. Dark matter twists the polarization into a 'B-mode' signal at arc minute scales on the sky. The measurement rejects the possibility of zero B-mode power from gravitational lensing at 97.5% confidence (<a href=\"http://arxiv.org/pdf/1403.2369v1.pdf\">arxiv:1403.2369</a>). This measurement was produced by the first year of POLARBEAR observations. This announcement came one week before the BICEP2 team announced their detection of B-mode power at degree scales, consistent with an energy scale for cosmic inflation of about 10<sup>16</sup> GeV. This was an exciting week for Cosmology! The figure below shows the BICEP2 and POLARBEAR measurements."); ?></p><center><img src="/respond/sites/polarbearv2/files/figure1.jpg" style="height:300px;"/></center><p id="1396587055"><?php print _("These remarkable results add to the compelling science case for precision characterization of the CMB B-mode angular power spectrum. POLARBEAR’s continued observations, and the POLARBEAR-2 and Simons Array upgrades, are on track to lead this detailed characterization. These instruments all have the angular resolution necessary to characterize the B-mode signal from gravitational lensing. This provides the opportunity to probe the physics of neutrinos and dark energy, and also allows the removal of the lensing “foreground” signal to enable the precision characterization of the primordial inflationary B-mode signal. With the ability to measure 80% of the sky from our Chilean site, we will be able to make precise measurements of inflationary physics, probing grand unified theory (GUT) energy scales."); ?></p><p id="31114-paragraph-4"><?php print _("The data from the first season of POLARBEAR data can be found at <a href=\"http://lambda.gsfc.nasa.gov/product/suborbit/polarbear_prod_table.cfm\">http://lambda.gsfc.nasa.gov/product/suborbit/polarbear_prod_table.cfm</a>"); ?></p></div></div>

</div>
</div>
        
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
  <script src="../js/bootstrap.min.js"></script>
  <script src="../js/docs.min.js"></script>
</body>
</html>

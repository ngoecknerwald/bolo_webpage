<?php 
$rootPrefix="../";
$siteUniqId="5334aac3933db";
$siteFriendlyId="polarbearv2";
$pageUniqId="533e2eae6d25f";
$pageFriendlyId="100609";
$pageTypeUniqId="533e2e398fae0";
$language="en";
include '../site.php';
?><!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="<?php print _("Measuring the CMB Polarization with POLARBEAR"); ?>">
	<meta name="author" content="Thomas Tu">
	<link rel="shortcut icon" href="assets/ico/favicon.ico">

	<title>POLARBEAR</title>

  <link href="../css/carousel.css" rel="stylesheet">
    <link href="//netdna.bootstrapcdn.com/bootswatch/3.0.0/slate/bootstrap.min.css" rel="stylesheet">

	<!-- css -->
<link href="../css/content.css?v=2.8.3" type="text/css" rel="stylesheet" media="screen">
<link href="//cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.css" type="text/css" rel="stylesheet" media="screen">
<link href="//netdna.bootstrapcdn.com/font-awesome/4.0.2/css/font-awesome.css" rel="stylesheet">
<link href="../css/prettify.css?v=2.8.3" type="text/css" rel="stylesheet" media="screen">
</head>

<body data-siteuniqid="5334aac3933db" data-sitefriendlyid="polarbearv2" data-domain="localhost:8888/respond/sites" data-pageuniqid="533e2eae6d25f" data-pagefriendlyid="100609" data-pagetypeuniqid="533e2e398fae0" data-api="localhost:8888/respond">

<!--Menu-->
<div class="navbar-wrapper">
<div class="container">

	<div class="navbar navbar-inverse navbar-static-top" role="navigation">
	<div class="container">
		<!--Make the menu mobile friendly-->
		<div class="navbar-header">
		<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
		<span class="sr-only"></span>
		<span class="icon-bar"></span>
		<span class="icon-bar"></span>
		<span class="icon-bar"></span>
		</button>
		<a class="navbar-brand" href="//localhost:8888/respond/sites">POLARBEAR WITH THE HUAN TRAN TELESCOPE | Measuring Polarization of the CMB at the James Ax Observatory</a>
		</div>
	
	<div class="navbar-collapse collapse">
	<ul class="nav navbar-nav">
		<li><?php print _("<a href=\"../index\">Home</a>"); ?></li><li><?php print _("<a href=\"../newsfeed\">Newsfeed</a>"); ?></li><li><?php print _("<a href=\"../technology\">Technology</a>"); ?></li><li><?php print _("<a href=\"../site\">Site</a>"); ?></li><li><?php print _("<a href=\"../future-plans\">Future Plans</a>"); ?></li><li><?php print _("<a href=\"../people\">People</a>"); ?></li><li><?php print _("<a href=\"../publications\">Publications and Outreach</a>"); ?></li><li><?php print _("<a href=\"../support\">Support</a>"); ?></li><li><?php print _("<a href=\"../gallery\">Gallery</a>"); ?></li><li><?php print _("<a href=\"../data\">Data</a>"); ?></li>
	</ul>
	</div>
	</div>
	</div>
</div>
</div>
  
  

<div class="panel panel-default">
<div class="panel-body">
	<div id="100609-block-2" class="block row" data-nested="not-nested" data-containerid="" data-containercssclass=""><div class="col col-md-3"><p id="100609-paragraph-1"><?php print _("<i class=\"fa fa-chevron-left\"></i> <a href=\"../newsfeed/index\">Go back to newsfeed</a>"); ?></p></div><div class="col col-md-9"><h3 id="100609-h3-2"><?php print _("TELESCOPE INSTALLATION COMPLETE AT CEDAR FLATS"); ?></h3><p id="100609-paragraph-2"><?php print _("The Polarbear telescope installation is now complete! The installers from GD-SATCOM have signed off on a working telescope. Polarbear is currently situated at PAD 25 amidst the CARMA array."); ?></p><img src="http://bolo.berkeley.edu/polarbear/sites/default/files/images/PBT_cedar_flat.preview.jpg" style="height:300px; " /></div></div>
</div>
</div>
        
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
  <script src="../js/bootstrap.min.js"></script>
  <script src="../js/docs.min.js"></script>
</body>
</html>

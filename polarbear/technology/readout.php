<?php 
$rootPrefix="../";
$siteUniqId="5334aac3933db";
$siteFriendlyId="polarbearv2";
$pageUniqId="533f02f7943e5";
$pageFriendlyId="readout";
$pageTypeUniqId="5335f99cd1e03";
$language="en";
include '../site.php';
?><!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="<?php print _("Measuring the CMB Polarization with POLARBEAR"); ?>">
	<meta name="author" content="Thomas Tu">
  <link rel="shortcut icon" href="/respond/sites/polarbearv2/files/acquia_slate_logo.png">

	<title>POLARBEAR</title>

  <link href="../css/carousel.css" rel="stylesheet">
    <link href="//netdna.bootstrapcdn.com/bootswatch/3.0.0/cerulean/bootstrap.min.css" rel="stylesheet">

	<!-- css -->
<link href="../css/content.css?v=2.8.3" type="text/css" rel="stylesheet" media="screen">
<link href="//cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.css" type="text/css" rel="stylesheet" media="screen">
<link href="//netdna.bootstrapcdn.com/font-awesome/4.0.2/css/font-awesome.css" rel="stylesheet">
<link href="../css/prettify.css?v=2.8.3" type="text/css" rel="stylesheet" media="screen">
</head>

<body data-siteuniqid="5334aac3933db" data-sitefriendlyid="polarbearv2" data-domain="bolo.berkeley.edu/polarbear" data-pageuniqid="533f02f7943e5" data-pagefriendlyid="readout" data-pagetypeuniqid="5335f99cd1e03" data-api="localhost:8888/respond">

<!--Menu-->
<div class="navbar-wrapper">
<div class="container">

	<div class="navbar navbar-inverse navbar-static-top" role="navigation">
	<div class="container">
		<!--Make the menu mobile friendly-->
		<div class="navbar-header" >
		<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
		<span class="sr-only"></span>
		<span class="icon-bar"></span>
		<span class="icon-bar"></span>
		<span class="icon-bar"></span>
		</button>
          <a class="navbar-brand" href="//bolo.berkeley.edu/polarbear">POLARBEAR WITH THE HUAN TRAN TELESCOPE | Measuring Polarization of the CMB at the James Ax Observatory</a>
          </div>
	
	<div class="navbar-collapse collapse">
	 <ul class="nav navbar-nav" style="width:90%; margin-left: 5%;">
  		<li><?php print _("<a href=\"../././\">Home</a>"); ?></li><li><?php print _("<a href=\"../newsfeed\">Newsfeed</a>"); ?></li><li><?php print _("<a href=\"../technology\">Technology</a>"); ?></li><li><?php print _("<a href=\"../site\">Site</a>"); ?></li><li><?php print _("<a href=\"../future-plans\">Future Plans</a>"); ?></li><li><?php print _("<a href=\"../people\">People</a>"); ?></li><li><?php print _("<a href=\"../publications\">Publications</a>"); ?></li><li><?php print _("<a href=\"../support\">Support</a>"); ?></li><li><?php print _("<a href=\"../data\">Data</a>"); ?></li>
       </ul>
      </div>
	</div> 
	</div>
</div>
</div>
  
  

<div class="panel panel-default">
<div class="panel-body">
<div id="readout-block-1" class="block row" data-nested="not-nested" data-containerid="" data-containercssclass=""><div class="col col-md-3"><p id="readout-paragraph-5"><?php print _("<i class=\"fa fa-chevron-left\"></i> <a href=\"./\">Back to technology page</a>"); ?></p></div><div class="col col-md-9"><h3 id="readout-h3-1"><?php print _("OVERVIEW"); ?></h3><p id="readout-paragraph-1"><?php print _("The readout system is another enabling technology, developed over many years specifically for CMB experiments. The polarbear readout allows the simultaneous recording of data from all pixels through a minimum number of wires. The system uses SQUIDS for their wide bandwidth and current sensitivity, paired with high speed digital signal processing.The reduction in wiring is accomplished by multiplexing many bolometer signals into a signal wire. The multiplexing comes in the form of placing the individual signals at different frequencies, much like different frequencies on the radio."); ?></p><h3 id="readout-h3-2"><?php print _("HOW IT WORKS"); ?></h3><p id="readout-paragraph-2"><?php print _("Large arrays of bolometric detectors require sophisticated readout schemes. To reduce thermal loading onto the coldest stages of the experiment and to reduce the complexity of instrumenting large arrays, the readout of these arrays is multiplexed. The readout multiplexing scheme that has been developed and demonstrated at Berkeley is frequency-domain multiplexing.Each sensor is biased with a sinusoidal voltage at a unique frequency. The sensor signals are thus separated in frequency space and can by summed before being readout by SQUID electronics (superconducting quantum interference device). Each sensor is placed in series with a tuned filter consisting of an inductor and a capacitor with values chosen to give center frequencies from 300 kHz to 1 MHz.<br>"); ?></p><h3 id="readout-h3-3"><?php print _("MULTIPLEXER"); ?></h3><p id="readout-paragraph-3"><?php print _("Pictured above is a schematic outlining the demonstration of an eight-channel readout multiplexer. Also shown is a picture of the inductor chip. The chips were manufactured at TRW-Northrop Grumman and each chip contains eight inductors."); ?></p><h3 id="readout-h3-4"><?php print _("READOUT"); ?></h3><p id="readout-paragraph-4"><?php print _("Pictured above is a spectrum across the readout multiplexer bandwidth showing Nyquist noise from the multiplexed sensors well above the expected readout noise. The sensor biased at 396 kHz sees an optical signal from a cooled LED and the adjacent sensor is monitored for crosstalk. The crosstalk is well below design requirements."); ?></p></div></div>

</div>
</div>
        
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
  <script src="../js/bootstrap.min.js"></script>
  <script src="../js/docs.min.js"></script>
</body>
</html>

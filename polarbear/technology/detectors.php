<?php 
$rootPrefix="../";
$siteUniqId="5334aac3933db";
$siteFriendlyId="polarbearv2";
$pageUniqId="5335f9c2b26fb";
$pageFriendlyId="detectors";
$pageTypeUniqId="5335f99cd1e03";
$language="en";
include '../site.php';
?><!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="<?php print _("Measuring the CMB Polarization with POLARBEAR"); ?>">
	<meta name="author" content="Thomas Tu">
  <link rel="shortcut icon" href="/respond/sites/polarbearv2/files/acquia_slate_logo.png">

	<title>POLARBEAR</title>

  <link href="../css/carousel.css" rel="stylesheet">
    <link href="//netdna.bootstrapcdn.com/bootswatch/3.0.0/cerulean/bootstrap.min.css" rel="stylesheet">

	<!-- css -->
<link href="../css/content.css?v=2.8.3" type="text/css" rel="stylesheet" media="screen">
<link href="//cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.css" type="text/css" rel="stylesheet" media="screen">
<link href="//netdna.bootstrapcdn.com/font-awesome/4.0.2/css/font-awesome.css" rel="stylesheet">
<link href="../css/prettify.css?v=2.8.3" type="text/css" rel="stylesheet" media="screen">
</head>

<body data-siteuniqid="5334aac3933db" data-sitefriendlyid="polarbearv2" data-domain="bolo.berkeley.edu/polarbear" data-pageuniqid="5335f9c2b26fb" data-pagefriendlyid="detectors" data-pagetypeuniqid="5335f99cd1e03" data-api="localhost:8888/respond">

<!--Menu-->
<div class="navbar-wrapper">
<div class="container">

	<div class="navbar navbar-inverse navbar-static-top" role="navigation">
	<div class="container">
		<!--Make the menu mobile friendly-->
		<div class="navbar-header" >
		<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
		<span class="sr-only"></span>
		<span class="icon-bar"></span>
		<span class="icon-bar"></span>
		<span class="icon-bar"></span>
		</button>
          <a class="navbar-brand" href="//bolo.berkeley.edu/polarbear">POLARBEAR WITH THE HUAN TRAN TELESCOPE | Measuring Polarization of the CMB at the James Ax Observatory</a>
          </div>
	
	<div class="navbar-collapse collapse">
	 <ul class="nav navbar-nav" style="width:90%; margin-left: 5%;">
  		<li><?php print _("<a href=\"../././\">Home</a>"); ?></li><li><?php print _("<a href=\"../newsfeed\">Newsfeed</a>"); ?></li><li><?php print _("<a href=\"../technology\">Technology</a>"); ?></li><li><?php print _("<a href=\"../site\">Site</a>"); ?></li><li><?php print _("<a href=\"../future-plans\">Future Plans</a>"); ?></li><li><?php print _("<a href=\"../people\">People</a>"); ?></li><li><?php print _("<a href=\"../publications\">Publications</a>"); ?></li><li><?php print _("<a href=\"../support\">Support</a>"); ?></li><li><?php print _("<a href=\"../data\">Data</a>"); ?></li>
       </ul>
      </div>
	</div> 
	</div>
</div>
</div>
  
  

<div class="panel panel-default">
<div class="panel-body">
<div id="readout-block-1" class="block row" data-nested="not-nested" data-containerid="" data-containercssclass=""><div class="col col-md-3"><p id="readout-paragraph-5"><?php print _("<i class=\"fa fa-chevron-left\"></i> <a href=\"./\">Back to technology page</a>"); ?></p></div><div class="col col-md-9"><h3 id="readout-h3-1"><?php print _("OVERVIEW"); ?></h3><p id="readout-paragraph-1"><?php print _("At the heart of the Polarbear lies the detector array. Polarbear can achieve such high sensitivity because it features a large imaging device, not unlike the CCD array found in digital cameras. The POLARBEAR array consists of 637 pixels (1274 antenna-coupled bolometers) on 7 hexagonal sub-arrays. Each of these sub-arrays is fabricated on a single 4'' silicon wafer in the Berkeley Nanolab. The POLARBEAR detectors integrate several features into a single pixel: polarization-sensitive antennas separate the signal into polarized components and couple these components from free space into superconducting microstrip waveguide, spectral bandpass filtering transmits only the desired frequency band, and superconducting TES thermistors on the thermally released bolometers provide the photon-noise limited detection of the transmitted electromagnetic wave."); ?></p><img src="/respond/sites/polarbearv2/files/picswithspec1_sm.jpg" style = "max-height: 300px;"/><h3 id="readout-h3-2"><?php print _("DETECTOR ARRAY"); ?></h3><p id="readout-paragraph-2"><?php print _("The array itself is composed of 7 hexagonal subarrays. Each subarray consists of 91 pixels, all of which are simultaneously fabricated in the Berkeley Microlab faciliy. The pixels themselves are a novel design incorporating a lenslet, antenna, filter, and superconducting detector. Each pixel is sensitive to both linear polarizations allowing us to make polarized maps. The entire array is cooled to a mere 3/10 of a Kelvin above absolute zero by a succession of sophisticated refrigerators. <br>"); ?></p><p id="1396894097"><?php print _("Below we see (a) a photograph of the POLARBEAR focal plane. For scale, the outer frame is 25cm in diameter. Six of the hexagonal sub-arrays have single crystal silicon lenslets; the single array of white lenslets are made of alumina, which is similar in performance. (b) A photograph of a single-detector pixel with a dual- polarization crossed double-slot dipole antenna, mi- crostrip transmission lines, band-defining filters, and suspended Transition Edge Sensor (TES) bolometers. (c) A scanning electron micrograph of the bolometer, showing its thermally isolating silicon nitride suspension."); ?></p><img src="http://cosmology.ucsd.edu/~thomastu/polarbeartest/sites/default/files/Technology/Detector/Detector.jpg" style="max-height: 300px;"/><h3 id="readout-h3-3"><?php print _("ANTENNA"); ?></h3><p id="readout-paragraph-3"><?php print _("The antenna used in our detector is a crossed double slot dipole coupled to a contacting silicon synthesized-elliptical lens. This antenna/lens combination has been used extensively at sub-mm frequencies and can couple efficiently to typical telescope optics. The antenna is also linearly polarized and a dual linearly polarized version can be used. This is convenient in our application, since the polarization of the incident light is what we want to measure."); ?></p><h3 id="readout-h3-4"><?php print _("SUPERCONDUCTING MICROSTRIP"); ?></h3><p id="readout-paragraph-4"><?php print _("The antenna is connected to a transmission line, which is used to bring the incoming optical power to the detector. Conventional transmission line materials would be very high loss at these frequencies, which would be unacceptable for an experiment such as this where we are already looking for an extremely weak signal. However, superconducting microstrip is a very low loss transmission line that will work well in our application. A convenient choice of materials is niobium, which has the highest superconducting temperature of all the elements. Niobium microstrip was originally investigated decades ago for use as interconnects in ultra high speed superconducting digital circuits. The loss in niobium microstrip is quite small for frequencies up to about 600 to 700 GHz, which makes it well suited for our application."); ?></p><h3 id="detectors-h3-7"><?php print _("BAND DEFINING MICROSTRIP FILTERS"); ?></h3><p id="detectors-paragraph-7"><?php print _("An advantage of the use of microstrip to connect the antenna to the bolometer is that band defining microstrip filters can be integrated into the transmission line. In a conventional millimeter wave receiver, band defining filters are metal mesh off-chip optical filters. If several bands are required, several of these off-chip filters must be used. In our detectors, the filters are integrated on the chip and different pixels can easily have different frequency sensitivities. Increasing the level of integration will ease the scaling of current receivers to higher pixel counts. The bandpass filter is a standard resonant filter design: a shorted quarter-wavelength stub filter."); ?></p><h3 id="detectors-h3-8"><?php print _("BOLOMETERS"); ?></h3><p id="detectors-paragraph-8"><?php print _("A bolometer is fundamentally a place where radiative power is deposited that is in thermal contact with a thermistor of heat capacity C, all of which has a weak connection to a thermal bath. Cryogenic bolometers are the most sensitive detectors of mm-wave power, and can be so sensitive that the uncertainty in the measurement is limited by the quantum statistics of the photons themselves. In the case of POLARBEAR, the weak thermal link between the thermistor and the thermal bath is provided by a silicon nitride suspension, where the silicon substrate is etched away beneath the suspension. The bolometers are composed of a terminating resistor and a superconducting Transition Edge Sensor (TES) located on a leg isolated silicon nitride substrate. The incoming power on the superconducting microstrip is dissipated in the load resistor as heat, and the change in temperature is measured by the TES. In order to achieve the sensitivity we need, the bolometer must be thermally isolated by the silicon nitride legs and the bath temperature must be below 300 mK. This reduces the detector noise to below that in the incident light. TESs have many advantages over conventional semiconducting bolometers. Probably the most important one in our application is that the TES readout electronics can be multiplexed, so that the signal for more than one pixel can be brought out on each pair of wires. The bolometers are composed of a terminating resistor and a superconducting Transition Edge Sensor (TES) located on a leg isolated silicon nitride substrate. The incoming power on the superconducting microstrip is dissipated in the load resistor as heat, and the change in temperature is measured by the TES. In order to achieve the sensitivity we need, the bolometer must be thermally isolated by the silicon nitride legs and the bath temperature must be below 300 mK. This reduces the detector noise to below that in the incident light. TESs have many advantages over conventional semiconducting bolometers. Probably the most important one in our application is that the TES readout electronics can be multiplexed, so that the signal for more than one pixel can be brought out on each pair of wires. As the pixel count of bolometer arrays grows this becomes increasingly important."); ?></p></div></div>

</div>
</div>
        
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
  <script src="../js/bootstrap.min.js"></script>
  <script src="../js/docs.min.js"></script>
</body>
</html>
